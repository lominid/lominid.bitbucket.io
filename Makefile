default: folders tools
	cp rami.css build/
	./gloss rami.md > build/rami-with-examples.md
	create build/rami-with-examples.md -o build/rami.html -s rami.css
	tools/make_examples
	sed 's/<pre><code>/<pre>/g; s/<\/code><\/pre>/<\/pre>/g' build/rami.html > index.html

tools:
	if [ -e ~/bin/rami-ascii ] ; then cp ~/bin/rami-ascii tools/ ; fi
	if [ -e ~/bin/rami ] ; then cp ~/bin/rami tools/ ; fi

clean:
	if [ -e build ] ; then rm -r build ; fi
	rm rami.html

folders:
	mkdir -p build
	mkdir -p build/examples
	mkdir -p build/examples/ascii
	mkdir -p build/examples/ipa
