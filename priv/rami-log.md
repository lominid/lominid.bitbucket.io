# The more you deliberate on the minutae of your work, the more you dehabilitate and destroy.

## The greater syntax problem

### If nouns chain
Each next place needs to be marked explicity.
The default behaviour of a chained noun has to be either a quantified genetive OR a verb.
Explicit place marking is tedious, especially for such a fundamental language feature.

```
I find their red room
-> Ríba rama nùma nabu rati a limi
A grey sign wiht only one entry
-> Rumagimi lusimilabi mi nujiki i da rùfuma ta ká.
```

It could be said that this is more in line with the philosophy of the language, being thought of as capable for recalling dreams.
However, this is against the philosophy of a logical language, as the semantics are very, very fluffty/unpredictable.
Though it's more preferable to have a weak focus system than a redundant one in an autofocusing language.

### If nouns don't chain

This approach simplifies verb phrases, but forces you to wrap 'adjectives' in relative clauses.

```
I find their red room
-> Ríba rama nabu mi ráti a limi
A grey sign wiht only one entry
-> Rumagimi mi lúsimilabi mi nujiki mi í da ta rúfuma.`
```

Let me not lie the fact that I have to capture the number irks me.
I might go for chaining nouns, and just suffer the place indexes.

## The genetive problem
ruta isa rama = my pole
ruta a rama - my pole
ruta mi ísa rama = my pole

Dáma ba rama rúfuki a limi maju násima rúmi.
Damà ba rama limi rufùki ba rumì a ba nasìma.

Now there's a new genetive problem.
Consider:

`Níma hu ramà damu limi lumu`

You might think that this groups as follows:

`Níma [hu ramà] gunehi limi lumu = My dog moves from here to there`

But it actually groups as follows

`Níma [hu <ramà gunehi>] limi lumu = Of my dog moves from here to there`

There are bigger problems with this though.
Consider:

`Hu bi níba limi`

What does this mean?

* a) It's two nouns.
* b) It's one genetive verba and a noun.

This can actually be resolved by allowing particulate openers to take stres.

Possible resolutions for the former problem

* Add a separator after a genetive phrase.
* Add a separator before the genetive head and its adjectives.
* Add a new form of genetive that is two syllables long and can take tone.
* Disalow adjectives in genetive phrases.
* Force the genetive into one part of speech so that there are no inflectional ambiguities.
* Hava a 'simple genetive' which shifts the stress to the  word and takes no arguments and a complex genetive which takes the stress and precedes a noun.
* Make the genetive take initial stress in the verbal form, no stress as a noun and the nasal inflection as an adjective.
* Make 'hu' stressed and a postposition in the adjective form, and allow it to form simple genetives in other forms.

`Hu raikà píba lumu = It's of the angry person (hu won't form a simple adjective`
`Raikà piba hù damu = The home of the angry peson`
`Hu raikà piba hù damu = of the angry person't home`

Thank God for the machinations of the human mind.
This method doesn't break the meter, and is very elegant.
