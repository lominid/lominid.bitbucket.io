# Rami - public alpha version

Rami is another permutation of my engelang skeleton.

Here are its notable features:

* Open syllable structure.
I've made conlangs with open syllable structures before but they sounded crap.
* Stress based parts of speech.
* Relatively flexible word order.

Here are its main pitfals:

* The morphology is fairly basic.
* There are alot of minimal pairs.
* Grammatical stress is hard to hear.
I don't know whether it's unfamiliarity or practical impossibility.
It would take quite a while to distinguish word boundaries without stress on each word if possible.
* Doesn't contain much in the way of semantics like Latejami.

## Morphophonology

```
S = p d g l r n ks ps pf ts j/ʒ/
G = a/a~ɔ/ i/i~ɪ/ u/u~ʊ/ ai au e o y/ə/
V = a i u 
F = s f x/ʃ/ 
E = b t k m z v c/tʃ/ h/h~ʔ/

Morpheme = SG (SG)? (FV)* EV (FV)*
Prefix = SG (F|nf)V
Root = Prefix* Morpheme
Particles = EG (FV)*
```

'V', 'z', 'q', 'ts', 'pf', 'ps', 'ks' and 'c' aren't used natively (for now™) but are useful in loanwords.

Note that unstressed vowels needn't be articulated fully before the end of a word.
When reduced in rapid speech, 'a' can be realised as /ɔ/, 'u' can be realised as /ʊ/ and 'i' can be realised as /ɪ/.

When written at the end of a word 'e', 'o' and 'y' can represent the reduced versions of 'i', 'a' and 'u' respectively.
When written inside a word, they represent the sounds /e/, /o/ and /ə/ respectively.
Words terminal 'e's, 'o's and 'y's can also be pronounced using the three afformentioned inner realisations, as words can only end in 'a', 'i' or 'u' due to morphological constraints.
If you pronounce 'e', 'o' or 'y' in such a way, you will not be misunderstood.
These are not used natively (for now™), but are used in loanwords such as 'lojba' for 'lojban'.

Note that for now, /h/ and /ʔ/ are allophones.
This means that two 'h' initial words can be pronounced with the first as /h/ and the second as /ʔ/ if this makes things flow better.

This yields 360 two syllable roots and 1080 three syllable derivatives.
While reading, three syllable minimal pairs shouldn't be too hard to distinguish, as they're related to more distinct two syllable roots.

### A note on orthography
If you don't have diacritics available, vowel stress can be indicated by double letters.
In the diphthongs of 'a', only the 'a' is doubled.

## Grammar

### Nasal and stress based inflections

The syllables of nouns are not stressed, in normal situations.
In noisy or otherwise difficult situations, the final syllable can be stressed.

EXAMPLE:0

Verbs are stressed on the first syllable of the head word.

EXAMPLE:1

Adjectives are stressed on the last syllable and prenasalise the next onset.

Prenasalisation before stops consists simply of creating a nasal at that stop's place of articulation.
For plosives, the nasal is written as an /m/, for dentals an /n/ and for velar stops an /ŋ/.
Before dental stops and velar stops,xthe prenasalisation is written as an 'n'.

Before 'l', 'r' and 'n' the nasal is an /m/ and is written as such.

EXAMPLE:the-old-animal-eats

Custom conjunctions prenasalise the following word's onset and are stressed like verbs, on the first syllable of the head noun.

EXAMPLE:the-old-animal-eats-before-the-young-animal

### Adjectives

In a situation where grouping matters, adjectives will group rightwards.
Though such a thing shouldn't really happen.
Unlike languages such as Lojban with 'tanru' and moreso Toaq with serial verbs, all adjectives in Rami merely predicate on their noun.

EXAMPLE:the-red-house

Adjectives can take arguments.

EXAMPLE:the-war-loving-animal
EXAMPLE:the-sleepy-person

Adjectives placed before a verb act as though they consume that verb's clause.

EXAMPLE:the-house-was-red-long

Is the same as:

EXAMPLE:the-house-was-red

These adjectives can also take arguments

EXAMPLE:the-house-was-red-adder

### Clauses and sentences

#### Structure
The sentence level clause may start with a verb.
This clause is ended by a sentence starter, or a by subordinating verb, a feature which will be discussed later.

EXAMPLE:the-house-is-red

The sentence level clause may also assume a head final form.

EXAMPLE:the-house-is-red-head-final

This is the form all other clauses in the language take.

The propositional clause is by default a noun referring to, well, a proposition.
Whether or not the proposition is true on depends on it's parent verb.
The proposition below is opened by 'ma' and is closed by the verb 'ráti'.

EXAMPLE:i-know-that-the-house-is-red

The other clasue like constructs are relative clauses and properties, which will be discussed later.

Adjectives can precede any clause opener.
When this happens, the meaning is the same as if the adjective preceded the head verb.
See above if you've forgotten.

Normal clauses and property clause openers are stressed to turn them into verbs.

EXAMPLE:the-war-is-that-i-fight-you

#### Subordination

You can place a verb after a sentence to make the sentence behave as a clausal argument of said verb.
This process can continue indefinitely.

This is useful for alot of things, namely tense.

EXAMPLE:the-animal-eats

EXAMPLE:the-animal-ate

#### Secondary subordination

Normally, what subordination can only occur at the end of a sentence.
However, using 'bau', you can subordinate any closed clause.

EXAMPLE:the-animal-which-i-saw

The subordinating verb can take arguments, both in their regular form and as adder phrases.
The first argument of the subordinating verb is the clause that it is subordinating.

EXAMPLE:the-animal-which-i-saw-before-i-was-attacked

#### Note on head final sentence level clauses
I may drop head final sentence level clauses in favour of Toaq like prenexes.
With a prenex, you can introduce the nouns that are to be talked about before you begin a long sentence, often making it easier to understand.

### Relative clauses

Relative clauses are created using the particle 'mi' and come after the noun.
The relative pronoun is 'ta'.

EXAMPLE:i-must-help-the-person-that-helps-me-long

When gaps are left in the clause, the relative particle is assumed to be in the leftmost gap.

EXAMPLE:i-must-help-the-person-that-helps-me

When relative clauses are used with verbs, the relative particle refers to the verb's clause.

### Properties
Properties are very closely related to predicates.
In natural languages, the closest analogue to the propery is the infinitive.

Consider the sentence 'I try to swim'.

Here, we have two predicates: `[] tries to do [property]` and `[] swims`.
The 'try' part is in the normal form, but the 'swim' part has been altered slightly in order to signify a special relationship with the head verb.
The relationship varies greatly, but the infinitive part stays the same.
Most verbs that would take infinitives in english take a property in Rami.

EXAMPLE:i-try-to-swim-long

The property pronoun finds a place in the same way that the relative pronoun finds one.
Thus, the above sentence can be rewritten as:

EXAMPLE:i-try-to-swim

### Adder phrases

Any root can have it's leftmost argument filled in using an adder phrase.

I've already demonstrated this property with adjectives, so here I'll show how it works with verbs and nouns.

Using an adder phrase with a verb is essentially a way of changing the word order.
You may do this for stylistic reasons, or when you think of new information after finishing a phrase.

Here are a list of sentences, each using different sentence structures with the help of adder phrases.

EXAMPLE:adder/permutations

Some verbs, when used as nouns, benefit from the ability to easily specify the object.

EXAMPLE:i-must-help-the-one-that-helps-me-adder

### The genetive
The genetive is a predicate which translate to `[] is of [noun]`, where the noun is filled in.
The genetive has three versions

```
Hi = form a noun 'mine'
Hí = form a verb 'it's mine'
Hín = form an adjective 'my'
```

Genetive nouns are formed by prefixing 'hi' to the noun.

EXAMPLE:i-know-of-that

The adjective form of the genetive separates a series of consecutive nouns.
They are left grouping.

EXAMPLE:the-valley-of-the-shadow-of-death-final

The genetive verb is formed by prefixing 'hí' to the noun.

EXAMPLE:the-house-is-of-the-angry-person-final

### Numebrs and counting
The particle 'hu' starts a number phrase.
Number phrases are ended by a vowel change to either: 'a' or 'i'.

'A' creates a predicate meaning `[] refers to _ things`.

'I' indicates the ordinal, creating a predicate meaning `[] is _th in sequence []`.

```
s = 0
t/d = 1
n = 2
m = 3
r = 4
l = 5
x/j = 6
g/k = 7
f/v = 8
p/b = 9
```

#### Some examples
'Tau' allows you to multiply a number by a power of ten.

EXAMPLE:powers-of-ten

'Bau' allows you to create quotients.

EXAMPLE:five-over-two

'Sau' symbolises a decimal point.

EXAMPLE:thirty-one-point-five

'Lau' is the negative sign, and is placed at the start of the number phrase.

EXAMPLE:negative-three

'Kau' is placed at the start of the number phrase, unless there is a negative in which case it goes after the negative.
indicates that the resulting number is the reciprocal of the given number.

EXAMPLE:reciprocal-of-three

'Rai' separates multiple numbers, which are to be added together.

EXAMPLE:3000-and-a-third

As self-segmentation isn't contingent on spaces or punctuation, you can include them in numbers without any change in meaning.

EXAMPLE:spaces-in-a-number

#### Note on numbers in compounds
Compounds are made with numbers as if the compound is a loanword.
This means that the number isn't changed internally, it only has the realisation of the final phoneme altered.

For example, you could translate bicycle using the number 2 + the word for vehichle.

`Hunoruku = /hunɔruku/`

### Letters and spelling
Spellings begin with an 'hau' and are closed witn an 'hi'.

```
Cu = letter.
HV = vowel.
Ci = number.
Ca = A letter that also represents the end of a spelling phrase, without the need of 'hi'.
Hai = I
```

Diphthongs are spelled as compound letters, 'hai' = 'hahai'.

```
Haupudufuhi = PDF
Hauduhoguhi = DOG
Hauluhainuhukusuhi = LINUKS
```

Like numbers, spelling phrases behave as loanwords when they are to be made into compounds.

`Haupudufuhedaxunaku = /Hawpudufuhɪdaxunaku/ = PDF + document/read/text + tool = PDF reader`

### The passive
The passive is formed simply by doubling the first syllable of a root.

```
Guta = [] eats []
Guguta = [] is eaten by []
```

Here are some verbal examples.

EXAMPLE:i-am-annointed
EXAMPLE:i-am-restored

But how do you switch the third argument with the first?

Well, just triple the first syllable...

Just kidding.
I'm undecided on how best to implement this feature, so you can use or use the temporary prefix 'misi'.

### Anaphora
Rather than using a number of pronouns, use rami anaphora (like latejami).
To form anaphora, take the first syllable of the word you want to make and append 'hau'.
If the source word is a compound, take the syllable from the head noun.

`Gisami -> gihau`

If this syllable is a diphthong of 'a', the latter part is removed.

`Danfunauta -> nahau`

This possibly reduces the scope of loanwords, so I may remove it in the future.

### Those custom conjunctions

Custom conjunctions can be made out of a verb whose two arguments are clauses.
The resulting sentence is one where this verb predicates over two identical
clauses, except for the conjunct argument.
The first clause contains the left noun and the second clause contains the right

EXAMPLE:the-old-animal-eats-before-the-young-animal

Is the same as:

EXAMPLE:the-old-animal-eats-before-the-young-animal-long

### Loanwords

Loanwords can be butchered in using the extra sounds mentioned in the morphophonology section.
Internal consonant clusters don't necessarily break the morphology, so long as consonants obey the structure S\*F\*E F\*.

Keep in mind that the value of a loanword system there in the hypothetical situation where the language has to be: a) parsed or b) processed from a soundbite.
Of course, the same could be said for conlangs as a whole, but sometimes it's just not worth the effort to transliterate a forgein word.

You could just put the word in quotation marks, followed by a root which describes what type of thing it is.

`Burger -> 'burger' nabi / purgarha`

#### Some transliterations

```
Shinar = Jinarha
Telephone = Delefomi
Sargent = Tsarjenti
Telephone shop = Delefomirubi
Dublin = Duplinhi
```

### Compounds

Compounds are right grouping.

They are formed by prefixing the a version of a two syllable root onto another root.
The prefix versions are formed according to the following rules:

```
m -> nf
b -> f
t -> s
k -> x
```

Note that diphthongs are broken up by 'nf'.
The prefix of `nauba`, a root predicate meaning `[] trusts []` is `naunfa`.
It is pronounced as `na-un-fa (3 syllables)`.

To enchance semantic predictability, compounds must make essentially the same predications as that of the head word, save for propositions, nouns and properties that have been filled in.

#### Compounds with loanwords

Compounds are made with loanwords by altering the quality of the last vowel.
'U' is realised as /ə~ʊ/ and written as 'y'.
'I' is realised as /e~ɪ/ and written as 'e'.
'A' is realised as /o~ɔ/ and written as 'o'.

`Guneherubi -> /guneherubi/ = dog-shop`

If the last part of the loanword is a diphthong, the diphthong is broken up.

`Pikayrubi -> /pikaərubi/ = pig-shop/butcher`

### Parsing logic

```
root-phrase = negative? root
	| numerical-root
	| seplling-root
	| name-root
	| anaphora

name-root = BA root

verb = róot-phrase
	| genetive-verb
noun = root-phrase
	| genetive-noun

genetive-verb = HÍ noun-phrase
genetive-noun = HI noun-phrase

sentence = verb-phrase noun-phrase* verb-phrase*
verb-phrase = adjective-phrase* verb adder-phrase?
adjective-phrase = adjective a-phrase?
adder-phrase = HA noun-phrase
noun-phrase =
	genetive-adjective-phrase? quantifier? adjective-phrase* noun (adder-phrase | relative-clause)? (conjunction noun-phrase)?
	| clause

genetive-adjective-phrase = noun-phrase HÍN

clause = MA noun-phrase* verb-phrase subordinating-part-of-clause?
subordinating-part-of-clause = BAU noun-phrase* verb-phrase subordinating-part-of-clause?
relative-clause = MI noun-phrases* verb-phrase

utterance = (( ^ | starter ) ~ sentence )*
```

Copy the parser text into [pest.rs](https://pest.rs) to see it work.


#### Some things to note

You can't have both a relative clause and an inserted noun.

The parsing logic doesn't yet account for hierarchical structure.

## APPENDIX A - Lexicon

### Primary roots
```
daba = [] is disease
dabi = [] is [property] enough to do [property]
dabu = [] is the end of []
daiba = N/A
daika = N/A
daima = [] is a coin
daita = N/A
daka = [] is positive
daki = [] covers []
daku = [] is a text
dama = [] is at present/now/at the same time as []
dami = [] is a bird
damu = [] is a building
data = [] is a system
dati = [] is a day
datu = [] is masculine/male
dauba = [] tries to do [property]
dauka = [] sits on []
daumi = [] is a shadow
dauta = N/A
diba = [] is a barrier/wall
dibi = [] would be the case if [] were the case
dibu = [] is a professional
dika = [] is a destination
diki = [] is sharp
diku = [] hopes/pines for []
dima = [] likes []
dimi = [] is a hole/recess in the ground
dimu = [] is possible
dita = [] is a hand
diti = N/A
ditu = N/A
duba = [] is on the right
dubi = [] is wrong
dubu = [] moves through []
duka = [] falls from []
duki = [] is a head
duku = [] loses []
duma = [] is a 2D shape
dumi = [] is wicked/evil/cursed
dumu = [] lasts for [] amount of time
duta = [] occurs as quickly as []
duti = [] is behind []
dutu = N/A
gaba = [] is about/pertain to []
gabi = [] is a result
gabu = [] is young
gaiba = N/A
gaika = N/A
gaima = N/A
gaita = N/A
gaitu = [] smells []
gaka = N/A
gaki = [] is green
gaku = [] is contrasting or 'other'
gama = YOU 2ps.sing
gami = [] is a sound
gamu = [] causes []
gata = [] is 'just'/only true
gati = [] is nice
gatu = [] is a door/portal/gate
gauba = N/A
gaubi = [] does [property] again
gauka = N/A
gauma = N/A
gauta = N/A
giba = [] writes [] is far from []
gibi = [] is a property/characteristic/aspect
gibu = [] is more true than []
gika = [] is a town/settlement/village/city
giki = [] is a device for doing [property]
giku = [] is a circle
gima = [] is a color
gimi = [] is information
gimu = [] gives []
gita = [] is below []
giti = [] is rich in property []
gitu = [] is a large domesticated animal
guba = [] is a group
gubi = [] is a light source
gubu = [] is a mammal
guka = [] is the way in which [] is done/becomes true
guki = [] is in the east
guku = [] is a rock/stone
guma = [] is alone
gumi = [] is liquid stuff
gumu = [] waits for []
guta = [] eats []
guti = [] is God
laibi = [] is oil
gutu = [] is a cup
naimu = [] is a some kind of acquaintance of []
laba = [] is clean
labi = [] is white/bright
labu = [] loves []
laiba = N/A
laika = N/A
laima = [] is confused
laita = N/A
laka = [] is broad/wide/thick
lafaka = [] is far from []
laki = [] makes []
laku = [] is cold
lama = [] is fuel
lami = [] is small
lamu = [] is true
lata = [] is a gas
lati = [] is above []
latu = [] wants [] to be the case
lauba = [] is far away from []
lauka = N/A
lauma = N/A
lauta = [] is heavy
liba = [] is a swelling
libi = [] is free
libu = [] helps/benefits/aids []
lika = [] is true if [] is true/[] is necessary for []
liki = [] is a piece/part/element/quantum of []
liku = [] is a tube/tunnel
lima = [] is water
limi = [] is here/this
limu = [] controls []
lita = [] is a path/road
liti = [] is to the left
litu = [] is new
luba = [] is a man-made place
lubi = [] is negative
lubu = [] is normal
luka = [] ought to/should be the case
luki = [] is true because of []
luku = [] swims []
luma = [] excretes []
lumi = [] is the moon
lumu = [] is there/that
luta = [] is the corporeal element of []
luti = [] is the purpose of []
lutu = N/A
naba = [] is a child
nabi = [] is a food item
nabu = [] is a room
naibu = [] is in front of []
naika = N/A
naimi = [] is allowed to do [property]
naita = N/A
naka = [] is a victim
naki = [] acquires []
naku = [] is a tool/implement
nama = [] is an animal
nami = [] is the target of []
namu = [] is a year
nata = [] is a point in time
nati = [] is a net
natu = [] is what it is
nauba = [] trusts []
nauka = [] is able to do []
nauma = [] is an example of []
nauta = [] is sticky/adhering
niba = [] collapses/dies
nibi = [] is a tree/woody plant
nibu = [] is a branch of science concerned with []
nika = [] is grey
niki = [] is solid
niku = [] is a reptile
nima = [] is after []
nimi = to be in motion
nimu = [] must occur as a matter of fate
nita = [] knows []
niti = [] communicates with []
nitu = [] is before [] in a sequence
nuba = [] meets []
nubi = [] is meat
nubu = [] is sad
nuka = [] is a piece of furniture
nuki = [] is less true than []
nuku = [] is similar to []
numa = [] perceives []
numi = [] is thankful to []
numu = [] refers to more than one thing
nuta = [] is a sport
nuti = [] is a number
nutu = [] sleeps with []
paba = N/A
pabi = [] is a seed
pabu = [] is feminine/female
paiba = N/A
paika = N/A
paima = [] is a friend of []
paita = [] follows []
paka = N/A
paki = [] is old
paku = N/A
pama = N/A
pami = [] does [] wholly
pamu = [] is inside []
pata = [] is before []
pati = [] is an item of clothing
patu = [] utters []
pauba = N/A
pauka = [] guides/leads [] in way [property]
pauma = [] feels comfortable doing [property]
pauta = N/A
piba = [] is a person
pibi = [] laughs
pibu = [] serves function/role []
pika = [] is happy
piki = [] is strange
piku = [] is a weapon
pima = [] is the same as []
pimi = [] is in the north
pimu = N/A
pita = [] is yellow
piti = [] is before [] in a sequence
pitu = [] is an event
puba = N/A
pubi = [] loves []
pubu = [] is purple
puka = [] fears/is afraid of []
puki = [] is a little bit [proper
puku = N/A
puma = [] uses []
pumi = [] loves []
pumu = N/A
puta = [] is a computer
puti = [] sings []
putu = [] is a foot
raba = [] becomes/changes into []
rabi = [] is worth []
rabu = [] is large
raxabu = [] is so [property] that they satisfy [property]
raiba = N/A
raika = [] is angry at []
raima = N/A
raita = N/A
raka = [] is fire/flame
raki = [] is a plant
raku = [] avoids []
rama = ME 1ps.sing
rami = [] is a lanugage
ramu = [] is probably true if [] i true
rata = [] is art
rati = [] is red
ratu = [] attacks []
raubi = [] returns to doing []
rauka = N/A
rauma = [] is rain []
rauta = [] is still true
riba = [] finds []
ribi = [] is a natural location
ribu = [] holds []
rika = [] is a weather condition
riki = [] is an insect
riku = [] is a parent
rima = [] is calm/peaceful/safe
rimi = [] wears []
rimu = [] is a world/domain
rita = [] is warm
riti = [] is important/significant
ritu = [] is royal
ruba = [] is healthy
rubi = [] is a shop/market
rubu = [] is a sibling
ruka = [] is crushed material
ruki = [] is narrow
ruku = [] is a vehicle
ruma = [] is a surface
rumi = [] thinks []
rumu = [] is a book
ruta = [] originates in []
ruti = [] is a window
rutu = [] is eternal/infinite
```

### Secondary Roots
```
daxabi = [] is true enough for [] to be true
dasiba = [] is sick
paufama = [] assuages/calms/comforts/cools []
dafaba = [] curses []
daxiba = [] is a poison
dafabu = [] stops doing [property]
daxiki = [] is a blanket/duvet
daxiku = short story
dafuku = [] is a document
dasuku = [] reads []
dafami = [] flies
daximi = [] is the sky
dasimi = [] is blue
daximu = [] is a complex
dafumu = [] is a house
daxata = [] is a governmental system
dafata = [] organises/prepares []
dasita = [] is ordered/structured/orderly/logical
dasiti = [] is popular/en vouge among []
dauxata = [] is folded/bent around []
disiba = [] is closed/locked
dixibu = [] is a job
difabu = [] works
difaka = [] points towards []
dixiki = [] is a knife
difaki = [] cuts []
disimu = [] is open/unlocked
difata = [] catches []
disuta = [] can reach/touch []
duxabi = [] is true instead of [] being true (and it's unfortunate)
dufaka = [] lowers/descends upon []
dufuki = [] is a face
guxika = [] is an explanation
gufaka = [] does [property] in way []
dusiku = [] is mourning the loss of []
dufuma = [] is a 3D shape
dufami = [] loathes/hates/despises []
duximi = [] is excrement/crap
duxuta = [] occurs as slowly as []
gasiba = [] is relevant.
gasibi = [] is efficient/prosperous
gausibi = [] is extra/additiona/auxhiliary
gaxibu = [] is early
gaisatu = [] is a nose
gafaku = [] betrays/disobeys/commits treason against []
gasiku = [] is specific/distinct/particular among []
gaxama = 2ps.pl
gasita = [] is pendantic
gaxuti = [] is not nice
gasuti = [] is true instead of [] being true (and it's fortunate)
gafati = [] dwells/abodes in []
gixubu = [] increases in how much it does []
gisika = [] is municipal
gixika = [] is a council
gixiku = [] is a ring
gifaku = [] surrounds []
gisiku = [] is round
gifuku = [] is a ball
gifutu = [] is a pasture
diximi = [] is a valley
gasima = [] is colorful
gifami = [] learns
gisami = [] is a school
gixita = [] is the earth/soil
gisita = [] is brown
gifata = [] burries []
gufuba = [] is among  []
nufabu = [] has mercy on []
guxaba = [] is with []
guxibi = [] is the sun
gufabi = [] shines on []
gusibi = [] is bright/rich in light.
gufubu = [] is hair
guxibu = [] is milk
guxuki = [] is in the west
gusiku = [] is hard
guxiku = [] is a brick
gufami = [] drinks []
guxata = [] gorges on []
gusiti = [] is holy
gufuti - [] is a creed/faith
gusati - [] is a church
laxuba = [] is dirty
lasiba = [] is modest/good/holy
laxibi = [] is the truth
laifama = [] confounds/disorganises []
lafuki = [] is a clone
laxima = [] is oil
lafumu = [] is a fact.
lafati = [] jumps/leaps []
lasati = [] is the paragon/summit/top of []
lausita = [] is sturdy/well built/reliable
lauxata = [] is overly/too/excessively [property] to do [property]
laufata = [] carries/hauls []
lixiba = [] is a cancerous growth
lixiki = [] is a dot/point
lifuki = [] is a body part
lisima = [] is wet
lixima = [] is a cloud
lifuma = [] is a body of water
lixami = 3ps.pl
lifaxita = [] is a choice
lifata = [] choses []
lixata = [] is a fork
lufumi = [] is a month
lusimi = [] is dark/black
luxami = [] is the cosmos
luxamu = 3ps.pl
lufati = [] is successful thanks to []
nasiba = [] is foolish/idiotic
nafaba = [] gives birth to []
nasabu = [] is a hotel/hostel
naxika = [] is an injury
naxiki = [] is a gift/present
nafaki = [] awards/endowns [] upon []
naxiku = [] is a component
nasima = [] is alive
naxima = [] is a dog
naxiti = [] is a basket
naxitu = [] is the news
naufaka = [] is adept at doing []
nausuma = [] backs/is evidence of []
nisuba = [] is death
nifaba = [] sleeps
nisiba = [] is lazy/lethargic
nixibi = [] is wood
nisaki = [] is the floor/ground
nifaku = [] crawls
nifuma = [] is posterity
nifami = [] moves []
nixami = [] ambulates/walks/runs
nisumi = [] sends [] to []
nifumu = [] is a prophecy
niximu = [] is a law
nisita = [] is wise
nuxibi = [] is muscle
nusaka = [] is a fixture
nusiki = [] is weak/sickly
nuxiki = [] is a detail
nufiki = [] decreases in how much it does []
nuxiku = [] is a version/variant/model/kind/type
nufuma = [] is an image/sight
nuxima = [] is a mirror
nusima = [] is blatant/obvious/evident
nufamu = [] measures/records []
nusata = [] is a court/field/plain
nufata = [] plays with []
nuxita = [] is a competition/match/game
nusitu = [] is attractive
paxiki - [] is outdated/old fashioned/an anachronism
pafaki - [] remembers []
pasimi = [] is full
pafumu = [] a core/center/middle of []
pafiti = [] is creased
paxitu = [] is a name
pixiba = [] is mankind/people in general
pisibi = [] is funny
pixama = [] is in balance with []
pixumi = [] is in the south
puxima = [] is a method
puxika = [] is a ghost/apparition/spirit/soul
pusama = [] is a workshop
puximi = [] is a darling/loved one
pusuki = [] is a song
puxitu = [] is a leg
raxaba = [] changes [] in way [property]
raxibi = [] is currency
rafubi = [] is gold
rasibi = [] is expensive
raxubi = [] is frugal
rafabi = [] buys/purchases []
raixaka = [] screams at []
rasuka = [] burns []
raxiki = [] is a flower
raxama = 1p.pl
raximi = [] is a word
rasami = [] is a tounge
raxata = [] is a collection
rafuta = [] is a pattern/filgree/design
rafuti = [] is blood
rasatu = [] is war
rauxima = [] is a drop
raufuma = [] is a flood
rausima = [] flows
rausita = [] persists
rixaba = [] searches for []
rifuba = [] is a discovery/innovation
rixibi = [] is a country/land
risibi = [] is wild/undomesticated
rifabi = [] is at []
rixibu = [] is a container
rifubu = [] has []
rixika = [] is the air/atmosphere
rifaka = [] breathes/inhales
risaka = [] is outside []
rifaki = [] annoys []
rixiku = [] is a married couple
rifaki = [] marries []
rixita = [] is fire/flame
rixatu = [] reigns over []
ruxika = [] is sand
rufuka = [] is salt
rufaki = [] is next/near to []
ruxiki = [] is a string/rope
rusiki = [] is stiff/constrained/limited in aspect []
rusifuki = [] is the limit
ruxiku = [] is a horse
rufama = [] is on []
risima = [] is flat
rixama = [] reclines on []
ruxima = [] is the horizon.
ruximi = [] is a thought/idea
rusami = [] is the mind
rufami = [] believes/opines []
ruximu = [] is a chapter
ruxita = [] is the beginning of []
rufata = [] starts to do []
```


### Compounds
```
raufigamu = [] returns [] to state []
risupiba = [] is a royal
dasuritu = [] is a king
pafuritu = [] is a queen
rixanimi = [] goes out of []
rixanixami = [] walks/runs out of []
gaufilaxirami = [] is an auxlang
risudaxata = [] is a monarchy
laxirami = [] is a conlang
lanfigisudibu = [] is a sheperd
lausarausima = [] overflows
dunfinaimu = [] is an enemy of []
dauxanuku = [] is a chair
gusigimu = [] annoints []
runfanuku = [] is a table
gusinauba = [] believes in []
ranfiraxaba = [] translates []
pifurumi = [] is sane
nusinuxiki = [] is a statistic
nusinuxikisi = [] is statistical
nisiputa = [] is a phone
danfunauta = [] is morter
pasiputa = [] is a laptop
nifanuma = [] dreams []
lusinita = [] understands []
lausagati = [] is salubrious/too good to be real or true
runfulubu = [] is a library
ranfinibu = [] is linguistics
laxaraxaba = [] scatters []
```

### Loanwords
```
pfantu = [] is an elefant
leku = [] is a horse
gunehi = [] is a dog
gauhi = [] is a cow
pikau = [] is a pig
padata = [] is a potato
lojba = [] is lojban
papelha = [] is Babel
lakolha = [] is alcohol
psalma = [] is a biblical psalm
```

#### Compounds with loanwords/numbers
```
hurodufuma = [] is a cube/box
hutodama = [] occurs as soon as [] occurs
guneherubi = [] is a dog shop
lixilojba = [] is a gismu
```
## APPENDIX B - Particles

### Clauses
```
bi = property clause
mi = relative clasue
ma = clause opener
bau = subordination particle
```

### Quoting
```
ba = start gramatically correct name.
mauxi = start name quote
mau = start quote
tasa = end quote (can take stress and nasality)
```

### Numbers
```
hu = start a number phrase, closed by a change to 'a' (quantitave) or 'i' (ordinal).

s = 0
t/d = 1
n = 2
m = 3
r = 4
l = 5
x/j = 6
g/k = 7
f/v = 8
p/b = 9

tau = to the exponent of (at the end, essentially between two numbers)
bau = over (between two numbers)
sau = decimal point (between two numbers)
kau = reciprocal (prefix)
rai = separator
lau = negative sign
```

### Letters and spelling
```
Ca = letter.
HV = vowel.
Cu = number.
Ci = A letter that also represents the end of a spelling phrase, without the need of 'hi'.
hai = I
```

### Conjunctions
```
ti = logical or
ku = logical and
kau = group and
```

### Quantifiers

Quantifiers come before the noun, and before any adjectives if adjectives are modifying that noun.
Genetive adjectives come before the quantifier.

```
bu = all
buxi = some
ki = no
ka = only
kai = definite
tu = question (which ...)
mai = generic
```

### Other
```
ta = relative clause relative pronoun
tau = property pronoun
misi = x1 <-\> x3
tai = negative, placed before a root
hi = make predicate which means 'of X'
ha = fills in the leftmost argument of the preceding morpheme
-hai = given either a prefix to form an interjection OR a simple syllable (closed class).
-hau = forms anaphora
```

### Sentence starters
```
mu = default starter
bai = order
hai = address
taxi = ultimately
tafu = questioning starter
bixa = therefore/in this case
masu = but/however/alas
mixi = in other words
```

## APPENDIX C - Affixes (are infixed to roots and appended to compounds)
Each word ought to have no more than three derivatives.
This is to prevent excessive minimal pairs.

Derivative 1

* xa = relation
* xu = adjective
* xi = noun

Derivative 2

* sa = noun
* su = relation
* si = adjective

Derivative 3

* fa = relation
* fu = noun
* fi = adjective

Tense words use derivative 2 for the perfect, 1 for the habitual and 3 for the imperfect.

## APPENDIX D - Examples

### Nibám paima
EXAMPLE:nibaampaima

#### Note
Making this kind of 'poetry' in Rami is halfway between sticking to a meter in english and making a Toaq pangram.
While there is no grammatical tone, there is grammatical stress.
Gramatical stress is less like tone in that the word at a particular part of the text is not stuck to a single part of speech.

#### Psalms 23 (Hunumàm psalma)
EXAMPLE:psalms-23

## Further reading
* [Toaq](https://toaq.org)
* [Lojban](https://lojban.org)
* [Latejami](http://www.rickmor.x10.mx/lexical_semantics.html)
